import logo from "@/public/svg/lgoo.svg";
import image from "@/public/img/beach.jpg";
import { Card } from "@/components/Card.jsx"

export default function Lab2() {

    const popularDestination = [
        {
            city: "Toronto",
            averagePrice: 120,
            propertyCount: 76,
            imageUrl: "https://dynamic-media-cdn.tripadvisor.com/media/photo-o/14/10/2f/e4/toronto.jpg?w=700&h=500&s=1",
            imageAlt: "Toronto skyline",
        },
        {
            city: "Malibu",
            averagePrice: 215,
            propertyCount: 43,
            imageUrl: "https://www.travelandleisure.com/thmb/_NNA_bUbYunEEXA2tGsV_OGphjg=/1500x0/filters:no_upscale():max_bytes(150000):strip_icc()/TAL-malibu-california-MILBEACHCA0623-f54a74656b6d4e3a8d91619972dd087f.jpg",
            imageAlt: "Cliff in Malibu",
        },
        {
            city: "Chicago",
            averagePrice: 130,
            propertyCount: 115,
            imageUrl: "https://www.travelandleisure.com/thmb/wwUPgdpCUuD5sAPFLQf4YasjH0M=/1500x0/filters:no_upscale():max_bytes(150000):strip_icc()/chicago-illinois-CHITG0221-e448062fc5164da0bba639f9857987f6.jpg",
            imageAlt: "Chicago skyline",
        },
        {
            city: "Seattle",
            averagePrice: 135,
            propertyCount: 63,
            imageUrl: "https://upload.wikimedia.org/wikipedia/commons/5/58/Seattle_Center_as_night_falls.jpg",
            imageAlt: "Seattle skyline",
        },
        {
            city: "Colorado",
            averagePrice: 85,
            propertyCount: 47,
            imageUrl: "https://media.cntraveler.com/photos/59e7869906aef90bf9358799/4:3/w_4848,h_3636,c_limit/Telluride_GettyImages-508568122.jpg",
            imageAlt: "Lake in Colorado",
        },
        {
            city: "Miami",
            averagePrice: 115,
            propertyCount: 86,
            imageUrl: "https://i.natgeofe.com/n/5de6e34a-d550-4358-b7ef-4d79a09c680e/aerial-beach-miami-florida_16x9.jpg",
            imageAlt: "Beach in Miami",
        },
    ];

    return (
        <main className="">
            <article className="lg:grid lg:grid-cols-2 2xl:grid-cols-5">
                <section className="px-8 py-12 max-w-md mx-auto sm:max-w-2xl lg:px-12 lg:py-24 lg:max-w-full xl:mr-0 2xl:col-span-2">
                    <div className="xl:max-w-xl">
                        <img src={logo.src} alt="logo" className="h-10" />
                        <img src={image.src} alt="Beach" className="mt-6 rounded-lg shadow-xl sm:mt-8 sm:h-64 sm:w-full sm:object-cover object-center lg:hidden" />
                        <h1 className="mt-6 text-2xl font-bold text-gray-900 sm:mt-8 sm:text-4xl lg:text-3xl xl:text-4xl">
                            You can work from anywhere.
                            <br className="hidden sm:inline" />
                            <span className="text-indigo-500"> Take advantage of it</span>
                        </h1>
                        <p className="mt-2 text-gray-600 sm:mt-4 sm:text-xl">
                            Workcation helps you fin work-friendly rentals in beautiful locatins so you can enjoy some nice weather even when you are not on vacation
                        </p>
                        <div className="mt-4 sm:mt-6 space-x-1">
                            <a href="#" className="btn btn-primary shadow-lg hover:-translate-y-0.5 transform transition">Book your scape</a>
                            <a href="#" className="btn btn-secondary">Learn more</a>
                        </div>
                    </div>
                </section>

                <div className="hidden relative lg:block 2xl:col-span-3">
                    <img src={image.src} alt="Beach" className="absolute inset-0 w-full h-full object-cover object-center" />
                </div>
            </article>

            <article className="max-w-md sm:max-w-xl lg:max-w-6xl mx-auto px-8 lg:px-12 py-8">
                <h2 className="text-xl text-gray-900">Popular destinations</h2>
                <p className="mt-2 text-gray-600">A selection of great work-friendly cities with lots to see and explore.</p>
                <section className="mt-6 grid gap-6 lg:grid-cols-2 xl:grid-cols-3">
                    {popularDestination.map(({ city, averagePrice, propertyCount, imageUrl, imageAlt }) => (
                        <Card key={city} city={city} averagePrice={averagePrice} propertyCount={propertyCount} imageUrl={imageUrl} imageAlt={imageAlt} />
                    ))}
                </section>
            </article>
        </main>
    )
}