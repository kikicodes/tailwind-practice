export default function Lab2Layout({ children }) {
    return (
        <main className="w-screen h-screen bg-gray-100">
            {children}
        </main>
    )
}