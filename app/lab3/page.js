import logo from "@/public/svg/lgoo.svg";
import image from "@/public/img/beach.jpg";

export default function Lab2() {
    return (
        <main className="">
            <article className="lg:grid lg:grid-cols-2 2xl:grid-cols-5">
                <section className="px-8 py-12 max-w-md mx-auto sm:max-w-2xl lg:px-12 lg:py-24 lg:max-w-full xl:mr-0 2xl:col-span-2">
                    <div className="xl:max-w-xl">
                        <img src={logo.src} alt="logo" className="h-10" />
                        <img src={image.src} alt="Beach" className="mt-6 rounded-lg shadow-xl sm:mt-8 sm:h-64 sm:w-full sm:object-cover object-center lg:hidden" />
                        <h1 className="mt-6 text-2xl font-bold text-gray-900 sm:mt-8 sm:text-4xl lg:text-3xl xl:text-4xl">
                            You can work from anywhere.
                            <br className="hidden sm:inline" />
                            <span className="text-indigo-500"> Take advantage of it</span>
                        </h1>
                        <p className="mt-2 text-gray-600 sm:mt-4 sm:text-xl">
                            Workcation helps you fin work-friendly rentals in beautiful locatins so you can enjoy some nice weather even when you are not on vacation
                        </p>
                        <div className="mt-4 sm:mt-6">
                            <a href="#" className="inline-block rounded-lg px-5 py-3 shadow-lg bg-indigo-500 text-white uppercase tracking-wider font-semibold text-small sm:text-base">Book your scape</a>
                        </div>
                    </div>
                </section>


                <div className="hidden relative lg:block 2xl:col-span-3">
                    <img src={image.src} alt="Beach" className="absolute inset-0 w-full h-full object-cover object-center" />
                </div>
            </article>

        </main>
    )
}