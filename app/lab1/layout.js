export default function Lab1Layout({ children }) {
    return (
        <main>
            {children}
        </main>
    )
}