import logo from "@/public/svg/lgoo.svg";
import image from "@/public/img/beach.jpg";

export default function Lab2() {
    return(
        <main className="">
            <div className="px-8 py-12">
                <img src={logo.src} alt="logo" className="h-10"/>
                <img src={image.src} alt="Beach" className="mt-6 rounded-lg shadow-xl"/>
                <h1 className="mt-6 text-2xl font-bold text-gray-900">
                    You can work from anywhere. <span className="text-indigo-500">Take advantage of it</span>
                </h1>
                <p className="mt-2 text-gray-600">
                    Workcation helps you fin work-friendly rentals in beautiful locatins so you can enjoy some nice weather even when you are not on vacation
                </p>
                <div className="mt-4">
                    <a href="#" className="inline-block rounded-lg px-5 py-3 shadow-lg bg-indigo-500 text-white uppercase tracking-wider font-semibold text-small">Book your scape</a>
                </div>
            </div>
        </main>
    )
}