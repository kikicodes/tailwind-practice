"use client"

import React from "react"

export function Card({city, averagePrice, propertyCount, imageUrl, imageAlt}) {
    return(
        <section>
            <div className="flex items-center rounded-lg bg-white shadow-lg overflow-hidden">
                    <img className="h-32 w-32 flex-shrink-0" src={imageUrl} alt={imageAlt}/>
                    <div className="px-6 py-4">
                        <h3 className="text-lg font-semibold text-gray-800">{city}</h3>
                        <p className="text-gray-600">${averagePrice} / night average</p>
                        <div className="mt-4">
                            <a href="#" className="text-[#0FA9E6] hover:text-[#3FBAEB] font-semibold text-sm">Explore {propertyCount} properties</a>
                        </div>
                    </div>
                </div>
        </section>
    )
}